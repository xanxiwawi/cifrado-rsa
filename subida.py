def upload_to_bucket(blob_name, path_to_file, bucket_name):

	storage_client = storage.Client.from_service_account_json('tel252-proyecto-1f836e2dcd9d.json')

	bucket = storage_client.get_bucket(bucket_name)
	blob = bucket.blob(blob_name)
	blob.upload_from_filename(path_to_file)

upload_to_bucket('informacionCifrada.txt', 'datosCifrados.txt', 'bucket_tel252')
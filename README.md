# Cifrado RSA

A continuacion tenemos un codigo que cifra datos mediante el algoritmo RSA, bajo el contexto donde un centro meteorologico recopilas temperaturas durante el dia, estos datos se cifran bajo una llave publica, y descifran bajo una llave privada.

Esto es parte del proyecto del ramo "Criptografia y Seguridad en la Información".

Profesora: Berioska Contreras
Integrantes:
-Benjamin Vallejos
-Eduardo Gomez

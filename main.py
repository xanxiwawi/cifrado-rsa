import random
#este codigo cifra mediante el metodo RSA.
#esta enfocado en la ecriptacion de numeros, siendo las temperaturas de nuestra problematica

#funcion encargada de verificar numeros primos
def verifica_primo(n):
	c=0
	x=2
	if n>=2:
		while x<=n/2:
			if n%x==0:
				c=c+1
				x=x+1
			else:
				x=x+1
		if c==0:
			return True
		else:
			return False
	else:
		return False

#funcion encargada de calcular el maximo comun divisor
def mcd(a,b):
	m=b%a
	while m!=0:
		b=a
		a=m
		m=b%a
	return a

#generar datos a cifrar
def generarTemperaturas():
    flujo=open("temperaturas.txt","w")
    #se asume que recoge datos cada 1 hora
    for e in range(24):
        flujo.write(str(random.randint(10, 35))+"\n")
    flujo.close()
    return "temperaturas.txt"

#funcion para cifrar mensaje
def cifrado(m,key):
    m=m.upper()
    cifr=[]
    valores="0123456789"
    for e in range(len(m)):
        for ee in range(len(valores)):
            if m[e]==valores[ee]:
                cifr.append((ee**key[1])%key[0])
    return cifr

def descifrado(m,key):
    desc=""
    valores="0123456789"
    for e in m:
        desc+=valores[(e**key[1])%key[0]]
    return desc


#recopilacion de p y q, como valores primos distintos
p=int(input("Ingrese valor de p: "))
while verifica_primo(p)==False:
    print("¡Tiene que ser primo!")
    p=int(input("Ingrese valor de p: "))
q=int(input("Ingrese valor de q: "))
while verifica_primo(q)==False or p==q:
    print("¡Tiene que ser primo distinto!")
    q=int(input("Ingrese valor de q: "))

#calculo de variables n y phi
n=p*q
phi=(p-1)*(q-1)

#calculo de valores posibles para e
e=2
l=[]
while e<phi:
    m=mcd(e,phi)
    if m==1:
        l.append(e)
        e+=1
    else:
        e+=1

#eleccion de valor de e
print(l)
e=int(input("Eliga un valor de la lista para asignar a e: "))
while e not in l:
    e=int(input("Eliga un valor de la lista para asignar a e: "))

#calculo de variable d
k=1
r=(1+k*phi)%e
while r!=0:
    k+=1
    r=(1+k*phi)%e
d=(1+k*phi)//e

#generar llave publica y privada
llavePublica=[n,e]
llavePrivada=[n,d]
print("Llave publica: ",llavePublica)
print("Llave privada: ",llavePrivada)

#generar datos simulados de temperatura bajo el contexto previo para su posterior cifrado
generarTemperaturas()
print("Temperaturas cifradas: ")
flujoMen=open("temperaturas.txt","r")
flujoCifr=open("datosCifrados.txt","w")
cifr=""
for linea in flujoMen:
    linea=linea.strip()
    lista=cifrado(linea,llavePublica)
    for e in lista:
        flujoCifr.write(str(e)+" ")
        cifr+=str(e)
    cifr+=" "
    flujoCifr.write("\n")
flujoCifr.close()
flujoMen.close()
print(cifr)

#descifrado de datos
flujoDesc=open("datosCifrados.txt","r")
desc=""
print("Temperaturas descifradas con nuestra llave privada: ")
for linea in flujoDesc:
    linea=linea.strip().split(" ")
    for e in linea:
        desc+=descifrado([int(e)],llavePrivada)
    desc+=" "
flujoDesc.close()
print(desc)

#prueba de cifrado y descifrado
#mensaje=input("Ingrese el mensaje a cifrar: ")
#print("Mensaje cifrado: ",cifrado(mensaje,llavePublica))
#print("Mensaje descifrado: ",descifrado(cifrado(mensaje,llavePublica),llavePrivada))
